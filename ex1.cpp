#include "Header.h"
#include <iostream>

void initQueue(queue* q, unsigned int size) {

	q->maxSize = size;
	q->elements = new unsigned int[size];
	q->rear = 0;
	q->front = q->rear;

}

void enqueue(queue* q, unsigned int newValue) {
	if (q->maxSize == q->rear) {
		printf("\nQueue is full\n");
		return;
	}

	q->elements[q->rear] = newValue;
	q->rear++;

}

int dequeue(queue* q)
{

	int element;

	if (q->front == q->rear) {
		printf("\nQueue is  empty\n");
		return -1;
	}

	// shift all the elements from index 2 till rear 
	// to the left by one 
	else {

		element = q->elements[0];

		for (int i = 0; i < q->rear - 1; i++) {
			q->elements[i] = q->elements[i + 1];
		}

		// decrement rear 
		q->rear--;

		return element;

	}
}

void cleanQueue(queue* q) {

	delete(q);
}